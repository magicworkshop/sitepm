package my.mymagic.academy.sitepm.API;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SyncAdapterType;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import my.mymagic.academy.sitepm.Model.User;
import my.mymagic.academy.sitepm.R;

/**
 * Created by User on 18-Oct-16.
 */

public class RestAPIClient {

    private static RestAPIClient sSharedInstance;

    private RequestQueue mRequestQueue;
    private String mUserToken;
    private SharedPreferences mSharedPreferences;


    // Private constructor for Singleton pattern
    private RestAPIClient(@NonNull Context context) {
        // We'll use this RequestQueue to dispatch our requests
        mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());

        mSharedPreferences = context.getSharedPreferences(context.getString(R.string.get_pref_key), Context.MODE_PRIVATE);
        mUserToken = loadPreference("USER-TOKEN");
    }

    // Get "singleton" instance using this static method
    public synchronized static RestAPIClient getInstance(Context context) {
        if (sSharedInstance == null) {
            sSharedInstance = new RestAPIClient(context);
            sSharedInstance.mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return sSharedInstance;
    }


    //To set the Header parameters + add user token if any
    private Request newRequest(int method, String url, JSONObject parameters, Response.Listener successListener, Response.ErrorListener errorListener) {

        //To perform a new request to Backendless
        BackEndlessJsonObjectRequest request = new BackEndlessJsonObjectRequest(method, url, parameters, successListener, errorListener);

        if (mUserToken != null) {
            request.putHeader("user-token", mUserToken);
        }

        return request;
    }

    //Get User Token from SharedParameters
    public String loadPreference(String preference) {

        String mValue = mSharedPreferences.getString(preference, null);

        //This is a test - just to show what is in the preference file
//        System.out.println("This is the start of listing what is in the preference file");
//        Map<String,String> mMap = (Map<String,String>) mSharedPreferences.getAll();
//        for (Map.Entry<String, String> entry : mMap.entrySet()) {
//            Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
//        }
//        System.out.println("This is the END");
        //End of test - listing of preference file

        return mValue;

    }


    //This method is to clear the Preference file
     private void clearPreference() {

         SharedPreferences.Editor editor = mSharedPreferences.edit();
         editor.clear();
         editor.commit();
     }

    //Save User Token in preference
    private void savePreference(String key, String value) {

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }


    public interface OnUserAuthenticationCompleteListener {
        void onComplete(User user, VolleyError error);
    }

    // Login
    public void login(String email, String password, String role,final OnUserAuthenticationCompleteListener completionListener) {
        String url = "https://api.backendless.com/v1/users/login";

        //To set the body
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("login", email);
            parameters.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // To combine the JSON request, Type, Header(newRequest method) + Body(parameters variable). The Header is part of the newRequest.
        Request request = newRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                User user = new User(response);

                //To clear the preference file to ensure clean sheet
                clearPreference();

                mUserToken = user.getUserToken();
                savePreference(user.getUSER_TOKEN(), mUserToken);
                savePreference(user.getUSER_NAME(), user.getName());
                savePreference(user.getUSER_HPHONE(), user.getHandPhone());
                savePreference(user.getUSER_ROLE(), user.getRole());
                savePreference(user.getUSER_LOGIN(), user.getLogin());

                UserController.getInstance().setLoggedInUser(user);

                //This is to test to request HPhone value from preference file
//                String temp = loadPreference(user.getUSER_HPHONE());
//                System.out.println("You have requested "+ user.getUSER_HPHONE() + " from Preference file " + temp);
                //End Test

                completionListener.onComplete(user, null);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                completionListener.onComplete(null, error);
            }
        });

        mRequestQueue.add(request);
    }

}
