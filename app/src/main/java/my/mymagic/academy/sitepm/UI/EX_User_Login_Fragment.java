package my.mymagic.academy.sitepm.UI;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import my.mymagic.academy.sitepm.R;

/**
 * Created by User on 22-Oct-16.
 */

public class EX_User_Login_Fragment extends Fragment {

    public static final String TAG = "EX_User_Login_Fragment";
    private EditText mUserET;
    private EditText mPasswordET;
    private String mUser;
    private String mPassword;
    //    String mHPhone;
//    String mTokenId;
//    String mRole = PM_ROLE;
    private Button mLogin_Button;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View mView = inflater.inflate(R.layout.fragment_ex_login_screen, container, false);

        mLogin_Button = (Button) mView.findViewById(R.id.fragment_ex_login_login_button);


        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //To add capturing of the GPS coordindate and link to the location geo fencing
        mLogin_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.activity_login_framelayout, new CheckInLocationFragment())
                        .addToBackStack(CheckInLocationFragment.TAG)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit();
            }
        });
    }
}
