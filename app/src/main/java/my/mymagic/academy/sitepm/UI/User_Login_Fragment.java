package my.mymagic.academy.sitepm.UI;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;

import my.mymagic.academy.sitepm.API.RestAPIClient;
import my.mymagic.academy.sitepm.API.UserController;
import my.mymagic.academy.sitepm.Model.User;
import my.mymagic.academy.sitepm.R;


/**
 * Created by User on 18-Oct-16.
 */

public class User_Login_Fragment extends Fragment {

    public static final String TAG = "User_Login_Fragment";

    private static final String EMPTY = "NO";
    private static final String PM_ROLE = "PM";
    private static final String EX_ROLE = "EX";

    private EditText mUserET;
    private EditText mPasswordET;
    private String mUser;
    private String mPassword;
//    String mHPhone;
//    String mTokenId;
//    String mRole = PM_ROLE;
    private Button mLogin_Button;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_login, container, false);

        mUserET = (EditText) mView.findViewById(R.id.fragment_login_et_login);
        mPasswordET = (EditText) mView.findViewById(R.id.fragment_login_et_password);
        mLogin_Button = (Button) mView.findViewById(R.id.fragment_login_login_button);

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLogin_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mUserET.getText().length() > 0 && mPasswordET.getText().length() > 5 ) {
                    mUser = mUserET.getText().toString();
                    mPassword = mPasswordET.getText().toString();

                    User_Login_Fragment.this.executeLogin(mUser, mPassword);
                    // Call a singleton to capture the login user information
                    // Set preference with the token id
                } else {
                    Snackbar.make(getView(), "Please ensure all fields above has values and Password minimal length is 6", Snackbar.LENGTH_LONG).show();
                }
            }
        });

    }


    public void executeLogin (String muser, String mpassword) {

        // Call the Volley and Singleton to BackEndless to check login and fetch number
        //Calendar.getInstance());

        String mURL = "https://api.backendless.com/v1/users/login";

        //Calling RestAPI with Volley to add new journal to Backendless storage
        RestAPIClient.getInstance(getContext()).login(muser, mpassword , PM_ROLE, new RestAPIClient.OnUserAuthenticationCompleteListener() {


            @Override
            public void onComplete(User user, VolleyError error) {
                if (error != null) {
                    NetworkResponse mNtwkResponse = error.networkResponse;
                    System.out.println ("Error Network Response Code :" + mNtwkResponse.statusCode);
                    String json = new String (mNtwkResponse.data);

                    if(json != null) {

                        switch (mNtwkResponse.statusCode) {

                            case 404:
                            case 422:
                            case 401:

                                Snackbar.make(getView(), json, Snackbar.LENGTH_LONG).show();

                                break;
                            default:
                                Snackbar.make(getView(), error.toString(), Snackbar.LENGTH_LONG).show();
                        }
                    }
                    else{
                        Snackbar.make(getView(), error.toString(), Snackbar.LENGTH_LONG).show();
                    }

                } else {

                    Snackbar.make(getView(), "Login Successful", Snackbar.LENGTH_LONG).show();

                    //To test the singleton UserController has captured the login user
//                    System.out.println(user2.getHandPhone());
//                    System.out.println(user2.getName());
//                    System.out.println(user2.getLogin());
//                    System.out.println(user2.getRole());
//                    System.out.println(user2.getUserToken());
                    //End of Test

                    getActivity().setResult(Activity.RESULT_OK);
                    getActivity().finish();



                }

            }

        });

    }


}
