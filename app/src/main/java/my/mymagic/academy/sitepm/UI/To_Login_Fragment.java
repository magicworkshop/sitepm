package my.mymagic.academy.sitepm.UI;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.ButtonBarLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import my.mymagic.academy.sitepm.API.UserController;
import my.mymagic.academy.sitepm.Model.User;
import my.mymagic.academy.sitepm.R;

/**
 * Created by User on 17-Oct-16.
 */

public class To_Login_Fragment extends Fragment {

    private static final String TAG = "To_Login_Fragment";
    private static final int MAIN2LOGIN_ACTIVITY = 50;
    private static final int RESULT_SUCCESS = 101;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mToLoginView = inflater.inflate(R.layout.fragment_to_login, container, false);

        return mToLoginView;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.startLoginActivity();

    }


    public void startLoginActivity () {
        Intent mProjectActivity = new Intent(getContext(),LoginActivity.class);
        startActivityForResult(mProjectActivity, MAIN2LOGIN_ACTIVITY);
    }

    //Upon the calling Activity finished and return to this calling fragement (Activity finish and return to calling Fragment)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, ": Click on Back");
        if (requestCode == MAIN2LOGIN_ACTIVITY) {

            switch (resultCode) {
                case Activity.RESULT_OK:
                    //Action:
                    //Check the user that login whether is a PM or EX
                    //Start the EX or PM fragement, and ends this fragement
                    User mUser = UserController.getInstance().getLoggedInUser();
                    if (mUser.getRole()=="PM") {
                        this.showPMProjectSummaryFragement();
                    } else {
                        this.showEXProjectSummaryFragement();
                    }

                    break;
                default:
                    //Action: Retrigger Login Activity
                    Snackbar.make(getView(), "The result for back is : " + resultCode, Snackbar.LENGTH_LONG).show();
                    this.startLoginActivity();

            }

        }

    }

    //To trigger PM Summary Fragment
    public void showPMProjectSummaryFragement() {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_to_login_vg_layout, new ProjectSummaryPM_Fragment())
                .addToBackStack(ProjectSummaryPM_Fragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();


        //Prepare Drawer layout menu items
//        getActivity().mNavigationView.inflateMenu(R.menu.main_activity_navigation_menu_file);


    }


    //To trigger PM Summary Fragment
    public void showEXProjectSummaryFragement() {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_to_login_vg_layout, new ProjectSummaryEX_Fragment())
                .addToBackStack(ProjectSummaryEX_Fragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }


}

