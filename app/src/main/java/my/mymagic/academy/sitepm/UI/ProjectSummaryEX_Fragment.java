package my.mymagic.academy.sitepm.UI;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.support.v4.app.Fragment;
import android.view.ViewGroup;

import my.mymagic.academy.sitepm.R;

/**
 * Created by User on 15-Oct-16.
 */

public class ProjectSummaryEX_Fragment extends Fragment {

    //logt <Tab>
    public static final String TAG = "ProjectSummaryEX_Fragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mProjSummEX = inflater.inflate(R.layout.fragment_project_summary_ex, container, false);
        return mProjSummEX;
    }
}
