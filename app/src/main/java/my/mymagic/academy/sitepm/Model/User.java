package my.mymagic.academy.sitepm.Model;

import android.support.annotation.NonNull;

import org.json.JSONObject;

/**
 * Created by User on 17-Oct-16.
 */

public class User {
    private String mLogin; //Email address
    private String mUserToken;
    private String mName;
    private String mHandPhone;
    private String mPassword;
    private String mRole;
    private final String USER_TOKEN = "user-token";
    private final String USER_NAME = "user-name";
    private final String USER_ROLE = "user-role";
    private final String USER_HPHONE = "user-hphone";
    private final String USER_LOGIN = "user-login";


    public User(@NonNull JSONObject jsonObject) {
        mLogin = jsonObject.optString("email");
        mHandPhone = jsonObject.optString ("Handphone");
        mName = jsonObject.optString("name");
        mUserToken = jsonObject.optString("user-token");
    }

    public User(){

    }

    public void clearUserInfo () {
        mLogin = null;
        mHandPhone = null;
        mName = null;
        mUserToken = null;
        mRole = null;
    }

    public String getHandPhone() {
        return mHandPhone;
    }

    public String getLogin() {
        return mLogin;
    } //Email address

    public String getName() {
        return mName;
    }

    public String getRole() {
        return mRole;
    }

    public String getUserToken() {
        return mUserToken;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setHandPhone(String handPhone) {
        mHandPhone = handPhone;
    }

    public void setRole (String role) {mRole = role;}

    public String getUSER_HPHONE() {
        return USER_HPHONE;
    }

    public String getUSER_LOGIN() {
        return USER_LOGIN;
    }

    public String getUSER_NAME() {
        return USER_NAME;
    }

    public String getUSER_ROLE() {
        return USER_ROLE;
    }

    public String getUSER_TOKEN() {
        return USER_TOKEN;
    }
}
