package my.mymagic.academy.sitepm.UI;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ViewGroup;

import my.mymagic.academy.sitepm.R;

/**
 * Created by User on 15-Oct-16.
 */

public class LoginActivity extends AppCompatActivity {

    private ViewGroup mContentViewGroup;
    private Toolbar mToolBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mContentViewGroup = (ViewGroup) findViewById(R.id.activity_login_framelayout);
        mToolBar = (Toolbar) findViewById(R.id.activity_login_toolbar);

        //To display the Toolbar
        setSupportActionBar(mToolBar);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.activity_login_framelayout, new Access_Fragment())
                .commit();


    }
}
