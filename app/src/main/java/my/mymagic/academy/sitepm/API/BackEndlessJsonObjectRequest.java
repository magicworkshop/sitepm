package my.mymagic.academy.sitepm.API;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by User on 18-Oct-16.
 */

public class BackEndlessJsonObjectRequest extends JsonObjectRequest {
    private final Map<String, String> mAdditionalHeaders = new HashMap<>();

    public BackEndlessJsonObjectRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    public void putHeader(String key, String value) {
        mAdditionalHeaders.put(key, value);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        // Set up required headers
        Map<String, String> headers = new HashMap<>(super.getHeaders());
        headers.put("application-id",  "D4EFA4B0-111A-48A5-FF24-A9CC73255600");
        headers.put("secret-key", "C6B280A9-6EE7-3677-FF3E-B42EB94C2E00");
        headers.put("application-type", "REST");

        headers.putAll(mAdditionalHeaders);

        //This is a TEST - to list out the JSON Hashmap
//        System.out.println("This is a JSON HASHMAP printout STARTS ");
//        for (Map.Entry<String,String> entry : headers.entrySet()) {
//            String key = entry.getKey();
//            String value = entry.getValue();
//            System.out.println(key + ":" + value);
//        }
//        System.out.println("This is a printout ENDS ");
        //End of Test

        return headers;

    }
}
