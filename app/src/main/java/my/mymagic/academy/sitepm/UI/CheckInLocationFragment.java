package my.mymagic.academy.sitepm.UI;

import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import my.mymagic.academy.sitepm.R;


/**
 * Created by fahmi.latip on 21/10/2016.
 */

public class CheckInLocationFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private TextView mCurrentTime;
    private TextView mCurrentDate;
    private TextView mUsername;
    private TextView mTask;
    private Button mCheckInBtn;
    private Button mCheckOutBtn;
    private SupportMapFragment mMapFragment;
    private GoogleMap mGoogleMap;
    private GoogleApiClient mGoogleApiClient;
    private Circle mLocationCircle;
    private Marker mLocationMarker;

    public static final String TAG = "CheckInLocationFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_checkin_location, container, false);

        mCurrentTime = (TextView) view.findViewById(R.id.fragment_current_time);
        mCurrentDate = (TextView) view.findViewById(R.id.fragment_current_date);
        mUsername = (TextView) view.findViewById(R.id.fragment_current_username);
        mTask = (TextView) view.findViewById(R.id.fragment_current_task);
        mCheckInBtn = (Button) view.findViewById(R.id.fragment_checkin_button);
        mCheckOutBtn = (Button) view.findViewById(R.id.fragment_checkout_button);

        Calendar calendar=Calendar.getInstance();

        calendar.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        //System.err.println("TIME ZONE " + calendar.getTimeZone().getDisplayName());
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(calendar.getTime());
        mCurrentDate.setText(formattedDate);

        if (savedInstanceState == null) {
            setupGoogleMapsFragment();
        } else {
            mMapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_current_location_vg_map);
    }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getContext(), this, this)
                    .addApi(LocationServices.API)
                    .build();
        }

        startLocationUpdates();
    }

    private void setupGoogleMapsFragment() {
        mMapFragment = SupportMapFragment.newInstance();

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_current_location_vg_map, mMapFragment)
                .commit();

        mMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mGoogleMap = googleMap;

                LatLng officeLatLng = new LatLng(4.2105, 101.9758);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(officeLatLng, 6);

                mGoogleMap.moveCamera(cameraUpdate);
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 100
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startLocationUpdates();
        }
    }

    private void startLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 100);
                return;
            }

            LocationRequest locationRequest = new LocationRequest();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(5000);

            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    locationRequest,
                    (com.google.android.gms.location.LocationListener) this
            );
        } else {
            Snackbar.make(getView(), "GoogleApiClient is not ready yet", Snackbar.LENGTH_LONG).show();
        }
    }

    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                (com.google.android.gms.location.LocationListener) CheckInLocationFragment.this
        );
    }

    @Override
    public void onLocationChanged(Location location) {
        Snackbar.make(getView(), "Location update received!", Snackbar.LENGTH_SHORT).show();

        LatLng userLatLng = new LatLng(location.getLatitude(), location.getLongitude());

        if (mLocationMarker == null) {
            MarkerOptions markerOptions = new MarkerOptions().position(userLatLng).title("Here I am!");
            mLocationMarker = mGoogleMap.addMarker(markerOptions);
        } else {
            mLocationMarker.setPosition(userLatLng);
        }

        if (mLocationCircle == null) {
            CircleOptions circleOptions = new CircleOptions().center(userLatLng).radius(location.getAccuracy());
            mLocationCircle = mGoogleMap.addCircle(circleOptions);
        } else {
            mLocationCircle.setCenter(userLatLng);
            mLocationCircle.setRadius(location.getAccuracy());
        }

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(userLatLng, 16);
        mGoogleMap.moveCamera(cameraUpdate);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        stopLocationUpdates();
    }
}
