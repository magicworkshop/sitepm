package my.mymagic.academy.sitepm.UI;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import my.mymagic.academy.sitepm.R;

/**
 * Created by User on 20-Oct-16.
 */

public class CreateNewUsersFragment extends Fragment {

    private static final String TAG = "CreateNewUsersFragment";
    Button mCreateUserButton;
    EditText mLogin;
    EditText mPassword;
    EditText mName;
    EditText mHPhone;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_create_new_user, container, false);

        mLogin = (EditText) mView.findViewById(R.id.fragment_create_new_user_et_login);
        mName = (EditText) mView.findViewById(R.id.fragment_create_new_user_et_name);
        mHPhone = (EditText) mView.findViewById(R.id.fragment_create_new_user_et_hphone);
        mPassword = (EditText) mView.findViewById(R.id.fragment_create_new_user_et_password);
        mCreateUserButton = (Button) mView.findViewById(R.id.fragment_create_new_user_btn_create);

        return mView;

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mCreateUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, ": Clicked on Create user");

                if (mLogin.length() > 0 && mName.length() > 0 && mHPhone.length() > 0 && mPassword.length() > 5) {
                    if (CreateNewUsersFragment.this.createNewUser()) {
                        Log.d(TAG, "onClick: Successed Created a New User.. do confirmed via email before it takes effect");
                    } else {
                        Log.d(TAG, "onClick: Failed create users ");
                    }

//                    Snackbar.make()
                }
            }
        });

    }


    public boolean createNewUser () {
        return true;
    }



}
