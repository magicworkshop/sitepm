package my.mymagic.academy.sitepm.Model;

import java.util.Calendar;

/**
 * Created by User on 22-Oct-16.
 */

public class Projects {
    private int mProjId;
    private String mProjectName;
    private String mDescription;
    private int mCreatedBy;
    private Calendar mCreatedOn;
    private Calendar mLastUpdateOn;

    public Projects(int projId, String projectName, String description, Calendar createdOn, int createdBy) {
        mProjId = projId;
        mProjectName = projectName;
        mDescription= description;
        mCreatedOn = createdOn;
        mCreatedBy = createdBy;
    }

    public String getProjectName() {
        return mProjectName;
    }

    public Calendar getCreatedOn() {
        return mCreatedOn;
    }

    public int getProjId() {
        return mProjId;
    }

}
