package my.mymagic.academy.sitepm.UI;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import my.mymagic.academy.sitepm.R;

/**
 * Created by User on 15-Oct-16.
 */

public class ProjectSummaryPM_Fragment extends Fragment {

    public static final String TAG = "ProjectSummaryPM_Fragme";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mProjSummPM = inflater.inflate(R.layout.fragment_project_summary_pm, container, false);
        return mProjSummPM;

    }
}
