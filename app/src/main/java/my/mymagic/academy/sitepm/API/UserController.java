package my.mymagic.academy.sitepm.API;

import my.mymagic.academy.sitepm.Model.User;

/**
 * Created by User on 19-Oct-16.
 */

public class UserController {

    private static UserController sUserController;

    private User mLoggedInUser;

    private UserController() {
        // Attempt to load a user
    }

    public static synchronized UserController getInstance() {
        if (sUserController == null) {
            sUserController = new UserController();
        }

        return sUserController;
    }

    public User getLoggedInUser() {
        return mLoggedInUser;
    }

    public void setLoggedInUser(User loggedInUser) {
        mLoggedInUser = loggedInUser;
    }


}
