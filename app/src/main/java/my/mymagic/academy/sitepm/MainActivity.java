package my.mymagic.academy.sitepm;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.ViewGroup;
import my.mymagic.academy.sitepm.UI.*;

import static java.security.AccessController.getContext;

/**
 * Created by User on 14-Oct-16.
 */

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {


    private static final String TAG = "MainActivity"; //logt <Tab>
    private static final String EMPTY = "NO";
    private static final String PM_ROLE = "PM";
    private static final String EX_ROLE = "EX";

    //To define Shared Preferences
    private SharedPreferences mSharedPreferences;

    //Define the Drawer Layout variables
    private DrawerLayout mDrawerLayout;
    private ViewGroup mContentViewGroup;
    private NavigationView mNavigationView;
    private Toolbar mToolBar;


    //User profile (to be incorporated into User model
    String mRole;
    String mTokenId;
    String mLogin; //aka email
    String mHPhone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //To check on whether is there a login already performed before that is stored in the Shared Preferences
        mSharedPreferences = getSharedPreferences(getString(R.string.get_pref_key), MODE_PRIVATE);
        mTokenId = mSharedPreferences.getString("Token_id", EMPTY);


        mTokenId = "7EDA53E8-F33D-B54D-FF2F-84E9DF6B2500";
        mRole = PM_ROLE;
        mHPhone = "0163338089";
        mLogin = "csleong268@outlook.com";
        mRole = EMPTY;

        //Set content View
        setContentView(R.layout.activity_main);

        //Assign the DrawerLayout variables to the layout
        mDrawerLayout = (DrawerLayout) findViewById(R.id.activity_main_drawerlayout);
        mContentViewGroup = (ViewGroup) findViewById(R.id.activity_main_framelayout);
        mNavigationView = (NavigationView) findViewById(R.id.activity_main_navigation_view);
        mToolBar = (Toolbar) findViewById(R.id.activity_main_toolbar);


        //To display the Toolbar
        setSupportActionBar(mToolBar);

        //Create ActionBar (with Hamburger logo)
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolBar, R.string.open_drawer, R.string.close_drawer);
        mDrawerLayout.addDrawerListener(drawerToggle); //To informed if the drawer is opened or closed
        drawerToggle.syncState(); //Display the hamburger logo

        //Prepare Drawer listener to listen to the menu selected
        mNavigationView.setNavigationItemSelectedListener(this);

//        if (mRole == PM_ROLE) {
//            //Prepare Drawer layout menu items
//            mNavigationView.inflateMenu(R.menu.activity_main_drawer_pm_menu);
//
//
//            //Display PM Project Summary fragment
//            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.activity_main_framelayout, new ProjectSummaryPM_Fragment())
//                    .commit();
//        } else if (mRole == EX_ROLE) {
//
//            //Prepare Drawer layout menu items
//            mNavigationView.inflateMenu(R.menu.activity_main_drawer_executor_menu);
//
//            //Display Executor Project Summary fragment
//            getSupportFragmentManager().beginTransaction()
//                    .add(R.id.activity_main_framelayout, new ProjectSummaryEX_Fragment())
//                    .commit();
//        }  else {

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.activity_main_framelayout, new To_Login_Fragment())
                    .commit();
//        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int mMenuItemId = item.getItemId();

        //Close the Drawer
        mDrawerLayout.closeDrawers();

        //To determine the menu for PM or EXECUTOR
        if (mRole == PM_ROLE) {
            //identify which drawer items was selected
            switch (mMenuItemId) {
                case R.id.menu_activity_main_pm_login:
                    Log.d(TAG, ": PM Login");
                    break;
                case R.id.menu_activity_main_pm_logout:
                    Log.d(TAG, ": PM Logout");
                    break;
                case R.id.menu_activity_main_pm_project_summary:
                    Log.d(TAG, ": PM Project Summary");
                    break;
                case R.id.menu_activity_main_pm_read_n_send_message:
                    Log.d(TAG, ": PM Read n Send Message");
                    break;
                case R.id.menu_activity_main_pm_swap_to_executor:
                    Log.d(TAG, ": PM swap to Executor");
                    break;
                case R.id.menu_activity_main_pm_todolist:
                    Log.d(TAG, ": PM To do List");
                    break;
                default:
                    Log.d(TAG, ": PM Invalid click");
            }
        }
        else if (mRole == EX_ROLE) {

            //identify which drawer items was selected
            switch (mMenuItemId) {
                case R.id.menu_activity_main_ex_login:
                    Log.d(TAG, ": EX Login");
                    break;
                case R.id.menu_activity_main_ex_logout:
                    Log.d(TAG, ": EX Logout");
                    break;
                case R.id.menu_activity_main_ex_my_projects:
                    Log.d(TAG, ": EX - My Projects");
                    break;
                case R.id.menu_activity_main_ex_read_n_send_message:
                    Log.d(TAG, ": EX Read n Send Message");
                    break;
                case R.id.menu_activity_main_ex_swap_to_pm:
                    Log.d(TAG, ": EX swap to PM");
                    break;
                case R.id.menu_activity_main_ex_todolist:
                    Log.d(TAG, ": PM To do List");
                    break;
                default:
                    Log.d(TAG, ": EX Invalid click");
            }
        }

        //This will close the drawer - to simulate after the selection, the drawer slides away
//        if (!mMenuSelectedFragment.equals(null)) {
//
//            getSupportFragmentManager()
//                    .beginTransaction()
//                    .replace(R.id.activity_main_vg_framelayout, mMenuSelectedFragment)
//                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                    .commit();
//        }

        return false;

    }

}