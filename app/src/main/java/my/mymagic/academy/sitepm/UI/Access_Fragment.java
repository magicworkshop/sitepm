package my.mymagic.academy.sitepm.UI;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import my.mymagic.academy.sitepm.R;

/**
 * Created by User on 17-Oct-16.
 */

public class Access_Fragment extends Fragment {

    private static final String TAG = "Access_Fragment";
    Button mPM_Button;
    Button mEX_Button;
    Button mNewUser_Button;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mAccessToLoginView = inflater.inflate(R.layout.fragment_access_to_login, container, false);

        mPM_Button = (Button) mAccessToLoginView.findViewById(R.id.access_fragment_pm_button);
        mEX_Button = (Button) mAccessToLoginView.findViewById(R.id.access_fragment_ex_button);
//        mNewUser_Button = (Button) mAccessToLoginView.findViewById(R.id.access_fragment_create_user_button);
        return mAccessToLoginView;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int mButtonPressed = 0;

        mPM_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, ": Clicked on PM Login Button");
                Access_Fragment.this.pm_Login();
            }
        });

        mEX_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, ": Clicked on EXECUTOR Login Button");
                Access_Fragment.this.ex_Login();
            }
        });

    }


    public void pm_Login () {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_login_framelayout, new User_Login_Fragment())
                .addToBackStack(User_Login_Fragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();

    }

    public void ex_Login () {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_login_framelayout, new EX_User_Login_Fragment())
                .addToBackStack(EX_User_Login_Fragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();

    }



}
